const rolePicker = document.getElementById('role-picker');
const gameDiv = document.getElementById('game');

rolePicker.addEventListener('submit', function(e) {
  e.preventDefault();
  const checkboxes = document.querySelectorAll('input[type="checkbox"]:checked');
  let roles = [];
  for (let checkbox of checkboxes) {
    roles.push(checkbox.name);
  }
  rolePicker.style.display = "none";
  PlayGame(roles);
});

const AUDIOCLIPS = [
  {audio: 'audio/Everyone Close Your Eyes.ogg', when:'always'},
  {audio: 'audio/Knight.ogg', when: 'knight'},
  {audio: 'audio/Chest-1.ogg', when: 'chest'},
  {audio: 'audio/Bad Guy.ogg', when: 'badguy'},
  {audio: 'audio/Pirate.ogg', when: 'pirate'},
  {audio: 'audio/Chest-2.ogg', when: 'chest'},
  {audio: 'audio/Good Guy.ogg', when:'goodguy'},
  {audio: 'audio/Hooded Figure.ogg', when: 'hoodedfigure'},
  {audio: 'audio/Everyone Open Your Eyes.ogg', when:'always'},
];

function PlayGame(roles, clip = 0) {
  gameDiv.innerHTML = '';
  gameDiv.style.display = "block";

  if (clip >= AUDIOCLIPS.length) {
    gameDiv.style.display = "none";
    rolePicker.style.display = "block";
    return
  }

  const clipdata = AUDIOCLIPS[clip];
  const when = clipdata.when;
  if (when == 'always' || roles.indexOf(when) != -1) {
    var audio = document.createElement('audio');
    var src = document.createElement('source');
    src.setAttribute('src', clipdata.audio);
    audio.appendChild(src);
    gameDiv.appendChild(audio);
    audio.play();
    audio.addEventListener("ended", function(e) {
      PlayGame(roles, clip+1);
    });
  } else {
    PlayGame(roles, clip+1);
  }
}
