Rules
-----

## NIGHT TIME

Knight - Wake up and guess who a bad guy is by placing a token on top of another player's card. If the knight's token is on your card during the night, skip your action and go back to sleep.
Chest - Wakes up and hides their own card to prevent any changes from happening to their role in the night.
Bad Guy - Wakes up and decides to shift everyone's card one space to the right, left or not at all.
Pirate - Wake up and shuffle all cards in a circle in front of you
Chest - Wakes up a second time to place their card back on the table in front of a random player.
Good Guy - Wakes up and checks one player's card.
Hooded Figure - Wakes up and switches the cards of two other players
Villager - Doesn't do anything at night.

## DAY TIME

Everyone wakes up and discusses who to vote for as the Bad Guy over some period of time (1-5minutes).

## VOTING

Everyone closes their eyes and then points at the player they want to vote for

## WINNING

Use your final card to determine the winners and losers.

Whichever player receives the most votes has their card revealed. If they are the Bad Guy, then Chest, Good Guy, and Villagers win. If they are _not_ the Bad Guy, then Bad Guy and Hooded Figure win.

In case of a tie:
 - If Bad Guy AND Hooded Figure are both playing, then the Villager/Chest/Good Guy win
 - Otherwise, Bad Guy and Hooded Figure win


